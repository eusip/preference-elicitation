# %% [markdown]
# # Linear Utility Problem

# This is a working deployment of the linear utility problem.

# %% [markdown]
#Load the following modules to run the linear utility problem.
import numpy as np

from Problem import LinearUtilityProblem
from Query import BoundQueryLinear, ComparisonQueryLinear
from Reasoner import Reasoner
from Sampler import MetropolisLinear

# %% [markdown]
# For analysis seed the random generator with a constant value.
np.random.seed(504)

# %% [markdown]
# Instantiate a flat utility problem as well as two questions. Then
# instantiate a reasoner and the sampler.
 
p = LinearUtilityProblem(4,4)
q1 = BoundQueryLinear(p, 0, .6)
q2 = ComparisonQueryLinear(p,(3,2),(1,0))
r = Reasoner(p)
s = MetropolisLinear(p, r)

# %% [markdown]
# A response is provided as well as a random walk for query 1 and query 2.
r.addPref(q1, 1.)
r.addPref(q2, 0.)
r.printPrefs()
s.maskParams()
print(str(s.x_mask))
print(str(s.y_mask))
# v = np.random.random((4, 4))
# print(str(v))

# s.takeWalk()
# print(str(s.walk))
r.printPrefs()
# s.takeWalk()
# print(str(s.walk))
# s.evalUtility()
