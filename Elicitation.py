class Elicitation():
    '''
    This class contains a number of simulations for query comparison.
    '''
    # don't use this function for EPU! call elicitEPUFlat() instead!
    def elicitFlat(p, q, r, s, u, queries, warmup, samples, 
                    sampler='metropolis', response='logistic', dist='uniform', 
                    q_type='EPU'):
        '''
        This function contains the question-response interaction for a 
        flat utility model.
        Parameters
        ----------
        p : obj
        The problem instance.
        q : obj
        The query instance.
        r : obj
        The reasoner instance.
        s : obj
        The sampler instance.
        u : obj
        The user instance.
        sampler : str
        The sampler to use for estimation. Either the metropolis sampler 
        is continuously used (metropolis), the metropolis sampler is run
         once and then the importance sampler is continuously used 
        (importance), or there is an alternation between the two 
        samplers for each query (mixture).
        q_type : str
        The queries used. Either all queries are bound, all queries are 
        comparison, or queries are selected based on EPU.
        questions : int
        The number of questions posed to the user.
        samples : int
        The number of samples using in the simulation. 
        '''
        s.initSamples(samples, dist=dist)
        print(str('Question 0'))
        p.computeUtilities(sampler=sampler, reset='y')
        p.printUtilities()
        u.computeUtility(utility='flat')
        print(str('Question 1'))
        if q_type == 'EPU':
            q.generateQueriesFlat()
            r.computeQuestion(response=response, sampler=sampler)
            p.printNQueries(5)
        if q_type == 'comparison':
            q.generateComparisonQueriesFlat()
        elif q_type == 'mixture':
            q.generateQueriesFlat()
            p.question = q.selectRandom()  # q.selectRandomBound()
        if q_type == 'comparison':
            p.question = q.selectRandomComp()
        elif q_type == 'bound':
            q.generateBoundQueriesFlat()
            p.question = q.selectRandomBound()
        u.answerQuestion(p.question)
        if q_type in ('bound', 'comparison', 'mixture'):
            q.updateQueries(p.question)
        else:  # EPU
            q.updateEPUQueries(p.question)
        p.updatePrefs()
        s.takeWalk(warmup, samples, response=response, init='metropolis', 
                    dist=dist)
        p.computeUtilities(sampler=sampler)
        p.printUtilities()
        u.computeUtility(utility='flat')
        print(str('Question 2'))
        if q_type == 'EPU':
            p.question = r.computeQuestion(response=response, sampler=sampler)
            p.printNQueries(5)
        elif q_type == 'comparison':
            p.question = q.selectRandomComp()
        elif q_type == 'mixture':
            p.question = q.selectRandom()  # q.selectRandomComp()
        elif q_type == 'bound':
            p.question = q.selectRandomBound()
        u.answerQuestion(p.question)
        if q_type in ('bound', 'comparison', 'mixture'):
            q.updateQueries(p.question)
        else:  # EPU
            q.updateEPUQueries(p.question)
        p.updatePrefs()
        s.takeWalk(warmup, samples, response=response, init='metropolis',
                    dist=dist)
        p.computeUtilities(sampler=sampler)
        p.printUtilities()
        u.computeUtility(utility='flat')
        for i in range(queries-2):
            # select a sampler # step not actually needed; redundant sampler call
            if sampler == 'metropolis':
                s.takeWalk(warmup, samples, response=response)
            elif sampler == 'importance':
                r.updateImportance(init=2, response=response)
            elif sampler == 'hybrid':
                if (len(p.preferences) % 2) == 0:
                    r.updateImportance(
                        init=len(p.preferences), response=response)
                else:
                    s.takeWalk(warmup, samples, response=response, 
                                init='importance')
            print(str('Question ') + str(i+3))
            # select a query
            if q_type == 'EPU':
                if sampler == 'metropolis':
                    r.computeQuestion(response=response, sampler='metropolis')
                elif sampler == 'importance':
                    r.computeQuestion(response=response, sampler='importance')
                elif sampler == 'hybrid':
                    if (len(p.preferences) % 2) == 0:
                        r.computeQuestion(response=response, 
                        sampler='importance')
                    else:
                        r.computeQuestion(response=response, \
                            sampler='metropolis')
                p.printNQueries(5)
            elif q_type == 'bound':
                p.question = q.selectRandomBound()
            elif q_type == 'comparison':
                p.question = q.selectRandomComp()
            elif q_type == 'mixture':
                if (len(p.preferences) % 2) == 0:
                    p.question = q.selectRandom()  # q.selectRandomBound()
                else:
                    p.question = q.selectRandom()  # q.selectRandomComp()
            u.answerQuestion(p.question)
            if q_type in ('bound', 'comparison', 'mixture'):
                q.updateQueries(p.question)
            else:  # EPU
                q.updateEPUQueries(p.question)
            p.updatePrefs()
            s.takeWalk(warmup, samples, response=response, init='metropolis', 
                        dist=dist)
            if sampler == 'metropolis':
                p.computeUtilities(sampler='metropolis')
            elif sampler == 'importance':
                p.computeUtilities(sampler='importance')
            elif sampler == 'hybrid':
                if (len(p.preferences) % 2) == 0:
                    p.computeUtilities(sampler='importance')
                else:
                    p.computeUtilities(sampler='metropolis')
            p.printUtilities()
            u.computeUtility(utility='flat')
    
    def elicitEPUFlat(p, q, r, s, u, queries, warmup, samples,
                   sampler='metropolis', response='logistic', dist='uniform', 
                   q_type='mixture'):
        '''
        This function contains the question-response interaction for a 
        flat utility model.
        Parameters
        ----------
        p : obj
        The problem instance.
        q : obj
        The query instance.
        r : obj
        The reasoner instance.
        s : obj
        The sampler instance.
        u : obj
        The user instance.
        sampler : str
        The sampler to use for estimation. Either the metropolis sampler 
        is continuously used (metropolis), the metropolis sampler is run
         once and then the importance sampler is continuously used 
        (importance), or there is an alternation between the two 
        samplers for each query (mixture).
        q_type : str
        The type of queries used. Either all queries are bound, all queries are 
        comparison, or either can be selected by maximizing EPU.
        questions : int
        The number of questions posed to the user.
        samples : int
        The number of samples using in the simulation. 
        '''
        s.initSamples(samples, dist=dist)
        if q_type == 'bound':
            q.generateBoundQueriesFlat()
        if q_type == 'comparison':
            q.generateComparisonQueriesFlat()
        if q_type == 'mixture':
            q.generateQueriesFlat()
        print(str('Question 0'))
        p.computeUtilities(sampler=sampler, reset='y')
        p.printUtilities()
        u.computeUtility(utility='flat')
        print(str('Question 1'))
        r.computeQuestion(response=response, sampler=sampler)
        p.printNQueries(5)
        u.answerQuestion(p.question)
        q.updateEPUQueries(p.question)
        p.updatePrefs()
        s.takeWalk(warmup, samples, response=response, init='metropolis', 
                    dist=dist)
        p.computeUtilities(sampler=sampler)
        p.printUtilities()
        u.computeUtility(utility='flat')
        print(str('Question 2'))
        r.computeQuestion(response=response, sampler=sampler)
        p.printNQueries(5)
        u.answerQuestion(p.question)
        q.updateEPUQueries(p.question)
        p.updatePrefs()
        s.takeWalk(warmup, samples, response=response, init='metropolis', 
                    dist=dist)
        p.computeUtilities(sampler=sampler)
        p.printUtilities()
        u.computeUtility(utility='flat')
        for i in range(queries-2):
            # select a sampler # step not actually needed; redundant sampler call
            if sampler == 'metropolis':
                s.takeWalk(warmup, samples, response=response,
                           init='metropolis', dist=dist)
            elif sampler == 'importance':
                r.updateImportance(init=2, response=response)
            elif sampler == 'hybrid':
                if (len(p.preferences) % 2) == 0:
                    r.updateImportance(
                        init=len(p.preferences), response=response)
                else:
                    s.takeWalk(warmup, samples, response=response,
                               init='importance', dist=dist)
            print(str('Question ') + str(i+3))
            # select a query
            if sampler == 'metropolis':
                r.computeQuestion(response=response,
                                  sampler='metropolis')
            elif sampler == 'importance':
                r.computeQuestion(response=response,
                                  sampler='importance')
            elif sampler == 'hybrid':
                if (len(p.preferences) % 2) == 0:
                    r.computeQuestion(response=response,
                                      sampler='importance')
                else:
                    r.computeQuestion(response=response,
                                      sampler='metropolis')
            p.printNQueries(5)
            u.answerQuestion(p.question)
            q.updateEPUQueries(p.question)
            p.updatePrefs()
            s.takeWalk(warmup, samples, response=response, init='metropolis', 
                        dist=dist)
            if sampler == 'metropolis':
                p.computeUtilities(sampler='metropolis')
            elif sampler == 'importance':
                p.computeUtilities(sampler='importance')
            elif sampler == 'hybrid':
                if (len(p.preferences) % 2) == 0:
                    p.computeUtilities(sampler='importance')
                else:
                    p.computeUtilities(sampler='metropolis')
            p.printUtilities()
            u.computeUtility(utility='flat')

    # don't use this function for EPU! call elicitEPULinear() instead!
    def elicitLinear(p, q, r, s, u, queries, warmup, samples,
                    sampler='metropolis', response='logistic', dist='uniform', 
                    q_type='EPU'):
        '''
        This function contains the question-response interaction for a 
        flat utility model.
        Parameters
        ----------
        p : obj
        The problem instance.
        q : obj
        The query instance.
        r : obj
        The reasoner instance.
        s : obj
        The sampler instance.
        u : obj
        The user instance.
        sampler : str
        The sampler to use for estimation. Either the metropolis sampler 
        is continuously used (metropolis), the metropolis sampler is run
         once and then the importance sampler is continuously used 
        (importance), or there is an alternation between the two 
        samplers for each query (mixture).
        q_type : str
        The queries used. Either all queries are bound, all queries are 
        comparison, or queries are selected based on EPU.
        questions : int
        The number of questions posed to the user.
        samples : int
        The number of samples using in the simulation. 
        '''
        s.initSamples(samples, dist=dist)
        print(str('Question 0'))
        p.computeUtilities(sampler=sampler, reset='y')
        p.printUtilities()
        u.computeUtility(utility='linear')
        print(str('Question 1'))
        if q_type == 'EPU':
            q.generateQueriesLinear()
            r.computeQuestion(response=response, sampler=sampler)
            p.printNQueries(5)
        if q_type == 'comparison':
            q.generateComparisonQueriesLinear()
        elif q_type == 'mixture':
            q.generateQueriesLinear()
            p.question = q.selectRandom()  # q.selectRandomBound()
        if q_type == 'comparison':
            p.question = q.selectRandomComp()
        elif q_type == 'bound':
            q.generateBoundQueriesLinear()
            p.question = q.selectRandomBound()
        u.answerQuestion(p.question)
        if q_type in ('bound', 'comparison', 'mixture'):
            q.updateQueries(p.question)
        else:  # EPU
            q.updateEPUQueries(p.question)
        if q_type in ('bound', 'comparison', 'mixture'):
            q.updateQueries(p.question)
        else:  # EPU
            q.updateEPUQueries(p.question)
        p.updatePrefs() 
        s.takeWalk(warmup, samples, response=response, init='metropolis', 
                    dist=dist)
        p.computeUtilities(sampler=sampler)
        p.printUtilities()
        u.computeUtility(utility='linear')
        print(str('Question 2'))
        if q_type == 'EPU':
            r.computeQuestion(response=response, sampler=sampler)
            p.printNQueries(5)
        elif q_type == 'comparison':
            p.question = q.selectRandomComp()
        elif q_type == 'mixture':
            p.question = q.selectRandom()  # q.selectRandomComp()
        elif q_type == 'bound':
            p.question = q.selectRandomBound()
        u.answerQuestion(p.question)
        if q_type in ('bound', 'comparison', 'mixture'):
            q.updateQueries(p.question)
        else:  # EPU
            q.updateEPUQueries(p.question)
        p.updatePrefs()
        s.takeWalk(warmup, samples, response=response, init='metropolis',
                    dist=dist)
        p.computeUtilities(sampler=sampler)
        p.printUtilities()
        u.computeUtility(utility='linear')
        for i in range(queries-2):
            # select a sampler # step not actually needed; redundant sampler call
            if sampler == 'metropolis':
                s.takeWalk(warmup, samples, response=response)
            elif sampler == 'importance':
                r.updateImportance(init=2, response=response)
            elif sampler == 'hybrid':
                if (len(p.preferences) % 2) == 0:
                    r.updateImportance(
                        init=len(p.preferences), response=response)
                else:
                    s.takeWalk(warmup, samples, response=response,
                               init='importance')
            print(str('Question ') + str(i+3))
            # select a query
            if q_type == 'EPU':
                if sampler == 'metropolis':
                    r.computeQuestion(response=response, sampler='metropolis')
                elif sampler == 'importance':
                    r.computeQuestion(response=response, sampler='importance')
                elif sampler == 'hybrid':
                    if (len(p.preferences) % 2) == 0:
                        r.computeQuestion(response=response,
                                          sampler='importance')
                    else:
                        r.computeQuestion(response=response,
                                          sampler='metropolis')
                p.printNQueries(5)
            elif q_type == 'bound':
                p.question = q.selectRandomBound()
            elif q_type == 'comparison':
                p.question = q.selectRandomComp()
            elif q_type == 'mixture':
                if (len(p.preferences) % 2) == 0:
                    p.question = q.selectRandomBound()
                else:
                    p.question = q.selectRandomComp()
            u.answerQuestion(p.question)
            if q_type in ('bound', 'comparison', 'mixture'):
                q.updateQueries(p.question)
            else:  # EPU
                q.updateEPUQueries(p.question)
            p.updatePrefs()
            s.takeWalk(warmup, samples, response=response, init='metropolis', 
                        dist=dist)
            if sampler == 'metropolis':
                p.computeUtilities(sampler='metropolis')
            elif sampler == 'importance':
                p.computeUtilities(sampler='importance')
            elif sampler == 'hybrid':
                if (len(p.preferences) % 2) == 0:
                    p.computeUtilities(sampler='importance')
                else:
                    p.computeUtilities(sampler='metropolis')
            p.printUtilities()
            u.computeUtility(utility='linear')       


    def elicitEPULinear(p, q, r, s, u, queries, warmup, samples,
                    sampler='metropolis', response='logistic', dist='uniform', 
                    q_type='mixture'):
        '''
        This function contains the question-response interaction for a 
        flat utility model.
        Parameters
        ----------
        p : obj
        The problem instance.
        q : obj
        The query instance.
        r : obj
        The reasoner instance.
        s : obj
        The sampler instance.
        u : obj
        The user instance.
        sampler : str
        The sampler to use for estimation. Either the metropolis sampler 
        is continuously used (metropolis), the metropolis sampler is run
         once and then the importance sampler is continuously used 
        (importance), or there is an alternation between the two 
        samplers for each query (mixture).
        q_type : str
        The type of queries used. Either all queries are bound, all 
        queries are comparison, or either can be selected by maximizing 
        EPU.
        questions : int
        The number of questions posed to the user.
        samples : int
        The number of samples using in the simulation. 
        '''
        s.initSamples(samples, dist=dist)
        if q_type == 'bound':
            q.generateBoundQueriesLinear()
        if q_type == 'comparison':
            q.generateComparisonQueriesLinear()
        if q_type == 'mixture':
            q.generateQueriesLinear()
        print(str('Question 0'))
        p.computeUtilities(sampler=sampler, reset='y')
        p.printUtilities()
        u.computeUtility(utility='linear')
        print(str('Question 1'))
        r.computeQuestion(response=response, sampler=sampler)
        p.printNQueries(5)
        u.answerQuestion(p.question)
        q.updateEPUQueries(p.question)
        p.updatePrefs()
        s.takeWalk(warmup, samples, response=response, init='metropolis', 
                    dist=dist)
        p.computeUtilities(sampler=sampler)
        p.printUtilities()
        u.computeUtility(utility='linear')
        print(str('Question 2'))
        r.computeQuestion(response=response, sampler=sampler)
        p.printNQueries(5)
        u.answerQuestion(p.question)
        q.updateEPUQueries(p.question)
        p.updatePrefs()
        s.takeWalk(warmup, samples, response=response, init='metropolis', 
                    dist=dist)
        p.computeUtilities(sampler=sampler)
        p.printUtilities()
        u.computeUtility(utility='linear')
        for i in range(queries-2):
            # select a sampler # step not actually needed; redundant sampler call
            if sampler == 'metropolis':
                s.takeWalk(warmup, samples, response=response,
                           init='metropolis', dist=dist)
            elif sampler == 'importance':
                r.updateImportance(init=2, response=response)
            elif sampler == 'hybrid':
                if (len(p.preferences) % 2) == 0:
                    r.updateImportance(
                        init=len(p.preferences), response=response)
                else:
                    s.takeWalk(warmup, samples, response=response,
                               init='importance', dist=dist)
            print(str('Question ') + str(i+3))
            # select a query
            if sampler == 'metropolis':
                r.computeQuestion(response=response,
                                  sampler='metropolis')
            elif sampler == 'importance':
                r.computeQuestion(response=response,
                                  sampler='importance')
            elif sampler == 'hybrid':
                if (len(p.preferences) % 2) == 0:
                    r.computeQuestion(response=response,
                                      sampler='importance')
                else:
                    r.computeQuestion(response=response,
                                      sampler='metropolis')
            p.printNQueries(5)
            u.answerQuestion(p.question)
            q.updateEPUQueries(p.question)
            p.updatePrefs()
            s.takeWalk(warmup, samples, response=response, init='metropolis',
                         dist=dist)
            if sampler == 'metropolis':
                p.computeUtilities(sampler='metropolis')
            elif sampler == 'importance':
                p.computeUtilities(sampler='importance')
            elif sampler == 'hybrid':
                if (len(p.preferences) % 2) == 0:
                    p.computeUtilities(sampler='importance')
                else:
                    p.computeUtilities(sampler='metropolis')
            p.printUtilities()
            u.computeUtility(utility='linear')
