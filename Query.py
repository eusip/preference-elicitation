# standard libraries
from itertools import combinations, count
import random

# third-party libraries
import numpy as np


class Query:
	'''
	p : The problem instance.
	'''
	problem = None
	
	def __init__(self, p):
		self.problem = p


class BoundQueryFlat(Query):
	'''
	This class defines bound utility queries for a flat utility model.
	Parameters
	----------
	p : obj
	The problem instance.
	x : int
	The alternative/item being considered.
	l : flt
	The utility level a query of the query.
	'''
	def __init__(self, p, x, l):
		super().__init__(p)
		# if isinstance(x, list):
		# 	if len(x) != self.problem.numItems():
		# 		raise IndexError('The dimension of the item set x '
		# 		'does not match the number of items in the problem.')
		self.x = x
		self.l = l

	def __hash__(self):
		return hash((self.x, self.l))

	def __eq__(self, other):
		return self.x == other.x and self.l == other.l

	def __str__(self):
		return "bound-query("+str(self.x)+", "+str(self.l)+")"


class ComparisonQueryFlat(Query):
	'''
	This class defines comparison utility queries for a flat utility 
	model.
	Parameters
	----------
	p : obj
	The problem instance.
	x, y : int
	The first and second alternatives/items being considered.
	'''
	def __init__(self, p, x, y):
		super().__init__(p)
		# if isinstance(x, list):
		# 	if len(x) != self.problem.numItems():
		# 		raise IndexError('The dimension of the item set x '
		# 		'does not match the number of items in the problem.')
		# elif isinstance(y, list):
		# 	if len(y) != self.problem.numItems():
		# 		raise IndexError('The dimension of the item set y '
		# 		'does not match the number of items in the problem.')
		self.x = x
		self.y = y

	def __hash__(self):
		return hash((self.x, self.y))

	def __eq__(self, other):
		return self.x == other.x and self.y == other.y

	def __str__(self):
		return "comparison-query("+str(self.x)+", "+str(self.y)+")"


class BoundQueryLinear(Query):
	'''
	This class defines bound utility queries for a linear utility model.
	Parameters
	----------
	p : obj
	The problem instance.
	x : int
	The global or local feature being considered. To consider all 
	features values (global query) pass the attribute row (e.g '1'). 
	To consider a specific alternative/item feature (local query) pass 
	the alterntive/item column as well in the form of a tuple 
	(e.g. '(1,0)').
	l : flt
	The utility level a query of the query.
	'''
	def __init__(self, p, x, l):
		super().__init__(p)
		# if isinstance(x, tuple):
		# 	if x[0] >= self.problem.numParams() or \
		# 		x[1] >= self.problem.numItems():
		# 		raise IndexError('One of the feature indices is '
		# 		'out of range.')
		# else:
		# 	if x >= self.problem.numParams():
		# 		raise IndexError('The feature index is out of range.')
		self.x = x
		self.l = l

	def __hash__(self):
		return hash((self.x, self.l))

	def __eq__(self, other):
		return self.x == other.x and self.l == other.l

	def __str__(self):
		return "bound-query("+str(self.x)+", "+str(self.l)+")"


class ComparisonQueryLinear(Query):
	'''
	p : obj
	The problem instance.
	x, y : int
	The global or local feature being considered. To consider all 
	feature values (global query) pass the attribute row (e.g '1'). 
	To consider a specific alternative/item feature (local query) pass 
	the alternative/item column as well in the form of a tuple 
	(e.g. '(1,0)'). If a local query is being posed then the first 
	element of the tuple must be the same for both alternatives/items.
	'''
	def __init__(self, p, x, y):
		super().__init__(p)
		# if isinstance(x, tuple):
		# 	if x[0] >= self.problem.numParams() or \
		# 		x[1] >= self.problem.numItems():
		# 		raise IndexError('One of the feature indices is '
		# 		'out of range.')
		# else:
		# 	if x >= self.problem.numParams():
		# 		raise IndexError('The feature index is out of range.')
		# if isinstance(y, tuple):
		# 	if (y[0] or y[1]) >= self.problem.numParams():
		# 		raise IndexError('One of the feature indices is '
		# 		'out of range.')
		# else:
		# 	if y >= self.problem.numParams():
		# 		raise IndexError('The feature index is out of range.')
		# if isinstance(x, tuple) and isinstance(y, tuple):
		# 	if x[0] != y[0]:
		# 		raise IndexError('The feature for the first item '
		# 		'does not match that of the second item.')
		self.x = x
		self.y = y

	def __hash__(self):
		return hash((self.x, self.y))

	def __eq__(self, other):
		return self.x == other.x and self.y == other.y

	def __str__(self):
		return "comparison-query("+str(self.x)+", "+str(self.y)+")"

class Engine:
	'''
	p : The problem instance.
	'''
	problem = None

	def __init__(self, p):
		self.problem = p 

	def generateQueriesFlat(self):
		'''
		This function generates a dictionary of possible queries for a 
		flat utility problem.
		'''
		self.problem.queries.clear()
		items = list(combinations(list(range(self.problem.numItems())), 2))
		# print(str(items))
		for item in items:
			self.problem.queries[ComparisonQueryFlat(
				self.problem, item[0], item[1])] = None
		bound = []
		# generate global bound queries
		# utility levels of .25, .5, and .75
		for i in range(self.problem.numItems()):
			for j in count(1):
				bound.append((i, round(j * .25, 2)))
				if j == 3:
					break
		# utility levels from .1 to .9 - 
		# for i in range(self.problem.numItems()):
		# 	for j in count(1):
		# 		bound.append((i, round(j * .1, 2)))
		# 		if j == 9:
		# 			break
		for params in bound:
			self.problem.queries[BoundQueryFlat(
				self.problem, params[0], params[1])] = None

	def generateBoundQueriesFlat(self):
		'''
		This function generates a dictionary of possible bound queries 
		for a flat utility problem.
		'''
		self.problem.queries.clear()
		bound = []
		# generate global bound queries
		# utility levels of .25, .5, and .75
		for i in range(self.problem.numItems()):
			for j in count(1):
				bound.append((i, round(j * .25, 2)))
				if j == 3:
					break
		# utility levels from .1 to .9 - 
		# for i in range(self.problem.numItems()):
		# 	for j in count(1):
		# 		bound.append((i, round(j * .1, 2)))
		# 		if j == 9:
		# 			break
		for params in bound:
			self.problem.queries[BoundQueryFlat(
				self.problem, params[0], params[1])] = None

	def generateComparisonQueriesFlat(self):
		'''
		This function generates a dictionary of possible comparison 
		queries for a flat utility problem.
		'''
		self.problem.queries.clear()
		items = list(combinations(list(range(self.problem.numItems())), 2))
		# print(str(items))
		for item in items:
			self.problem.queries[ComparisonQueryFlat(
				self.problem, item[0], item[1])] = None

	def generateQueriesLinear(self):
		'''
		This function generates a dictionary of possible queries for a 
		linear utility problem.
		'''
		self.problem.queries.clear()
		# comp_l = []
		bound = []
		comp_g = list(combinations(list(range(self.problem.numParams())), 2))
		# generate global comparison queries
		items = list(combinations(list(range(self.problem.numItems())), 2))
		# generate local comparison queries
		# for i in range(self.problem.numParams()):
		# 	for j in items:
		# 		comp_l.append(((i, j[0]), (i, j[1])))
		# generate global bound queries - utility levels of .25, .5, and .75
		for i in range(self.problem.numParams()):
			for j in count(1):
				bound.append((i, round(j * .25, 2)))
				if j == 3:
					break
		# utility levels from .1 to .9, step of .1
		# for i in range(self.problem.numParams()):
		# 	for j in count(1):
		# 		bound.append((i, round(j * .1, 2)))
		# 		if j == 9:
					# break
		comp = comp_g # + comp_l
		for params in comp:
			self.problem.queries[ComparisonQueryLinear(
				self.problem, params[0], params[1])] = None
		for params in bound:
			self.problem.queries[BoundQueryLinear(
				self.problem, params[0], params[1])] = None

	def generateBoundQueriesLinear(self):
		'''
		This function generates a dictionary of possible bound queries 
		for a flat utility problem.
		'''
		self.problem.queries.clear()
		bound = []
		# generate global bound queries
		# utility levels of .25, .5, and .75
		for i in range(self.problem.numParams()):
			for j in count(1):
				bound.append((i, round(j * .25, 2)))
				if j == 3:
					break
		# utility levels from .1 to .9, step of .1
		# for i in range(self.problem.numParams()):
		# 	for j in count(1):
		# 		bound.append((i, round(j * .1, 2)))
		# 		if j == 9:
		# 			break
		for params in bound:
			self.problem.queries[BoundQueryLinear(
				self.problem, params[0], params[1])] = None

	def generateComparisonQueriesLinear(self):
		'''
		This function generates a dictionary of possible comparison 
		queries for a flat utility problem.
		'''
		self.problem.queries.clear()
		# comp_l = []
		comp_g = list(combinations(list(range(self.problem.numParams())), 2))
		# generate global comparison queries
		items = list(combinations(list(range(self.problem.numItems())), 2))
		# generate local comparison queries
		# for i in range(self.problem.numParams()):
		# 	for j in items:
		# 		comp_l.append(((i, j[0]), (i, j[1])))
		comp = comp_g  # + comp_l
		for params in comp:
			self.problem.queries[ComparisonQueryLinear(
				self.problem, params[0], params[1])] = None

	def selectRandomComp(self):
		'''
		This function selections a random comparison query from the 
		initialized dictionary.
		'''
		while True:
			q = {k: self.problem.queries[k] for k in random.sample(
				self.problem.queries.keys(), 1)}
			if type(list(q.keys())[0]).__name__ not in \
				('ComparisonQueryFlat', 'ComparisonQueryLinear'):
				continue
			else:
				break
		# print(str(list(q.keys())[0]))
		return list(q.keys())[0]

	def selectRandomBound(self):
		'''
		This function selections a random bound query from the 
		initialized dictionary.
		'''
		while True:
			q = {k: self.problem.queries[k] for k in random.sample(
				self.problem.queries.keys(), 1)}
			if type(list(q.keys())[0]).__name__ not in \
				('BoundQueryFlat', 'BoundQueryLinear'):
				continue
			else:
				break
		# print(str(list(q.keys())[0]))
		return list(q.keys())[0]

	def selectRandom(self):
		'''
		This function selections a random query from the initialized 
		dictionary.
		'''
		q = {k: self.problem.queries[k] for k in random.sample(
			self.problem.queries.keys(), 1)}
		# print(str(list(q.keys())[0]))
		return list(q.keys())[0]

	# works for EPU comparison
	def updateEPUQueries(self, query):
		'''
		This function updates the initialized dictionary by removing a 
		previously asked question.
		'''
		try:
			del self.problem.queries[query]
		except KeyError:
			print('Query deletion unsuccessful.')

	# works for non-EPU comparisons
	def updateQueries(self, query):
		'''
		to be continued.
		'''
		if type(query).__name__ in ('BoundQueryFlat', 'BoundQueryLinear'):
			for i in self.problem.queries.keys():
				if type(i).__name__ in ('BoundQueryFlat', 'BoundQueryLinear'):
					if i.x == query.x and i.l == query.l:
						try:
							del self.problem.queries[query]
							break
						except KeyError:
							print('Query deletion unsuccessful.')
		elif type(query).__name__ in ('ComparisonQueryFlat', 'ComparisonQueryLinear'):
			for i in self.problem.queries.keys():
				if type(i).__name__ in ('ComparisonQueryFlat', 'ComparisonQueryLinear'):
					if i.x == query.x and i.y == query.y:
						try:
							del self.problem.queries[query]
							break
						except KeyError:
							print('Query deletion unsuccessful.')


