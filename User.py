# standard libraries
from functools import reduce
import math

# third-party libraries
import numpy as np


class User():
	'''
	p : obj
	The problem instance.
	'''
	problem = None

	def __init__(self, p):
		self.problem = p

	def generateProfile(self, utility):
		'''
		This function generates a sample of feature weights from a
		multivariate Guassian distribution over the weights of each 
		feature. This sample represents the utility profile of a 
		particular user from amongst a population of users with similar 
		utility profiles.
		Parameters
		----------
		utility : str
		This string denotes which utility type the function should 
		implicitly be called for. 
		'''
		# np.random.seed(555) # 3 items
		# np.random.seed(398) # 12 items
		np.random.seed(302) # 24 items
		# np.random.seed(642) # 48 items
		if utility == 'linear':
			self.mean = np.full((1, self.problem.numParams()), \
				self.problem.mean)
			while True:
				v = np.random.normal(self.mean, self.problem.sd)[0]
				if np.any(v >= 1.) or np.any(v < 0.):
					continue
				else:
					break
		else:  # flat 
			self.mean = np.full((1, self.problem.numItems()), \
				self.problem.mean)
			while True:
				v = np.random.normal(self.mean, self.problem.sd)[0]
				if np.any(v >= 1.) or np.any(v < 0.):
					continue
				else:
					break
		self.problem.profile = v

	def answerBound(self, query):
		'''
		This function generates a user response to global bound queries.
		Parameters
		----------
		query : obj
		This is either a BoundQueryFlat or BoundQueryLinear object.
		'''
		# if feature/item weight is greater than l yes; otherwise no
		if self.problem.profile[query.x] > query.l:
			v = 1.
		else:
			v = 0.
		self.problem.answer = v

	def answerComparison(self, query):
		'''
		This function generates a user response to global bound queries.
		A softmax response model is assumed.
		Parameters
		----------
		query : obj
		This is either a ComparisonQueryFlat or ComparisonQueryLinear 
		object.
		'''
		# if softmax is greater than .5 yes; otherwise no
		params = []
		# if (type(query.x) == tuple) and (type(query.y) == tuple):
		# local comparison query
		# 	mean_p = np.full((1, self.problem.numItems()), 
		# 					self.problem.profile[query.x[0]])
		# 	while True:
		# 		v = np.random.normal(mean_p, self.problem.sd)[0]
		# 		if np.any(v >= 1.) or np.any(v < 0.):
		# 			continue
		# 		else:
		# 			break
		# 	profile = v 
		# 	params.append(query.x[1])
		# 	params.append(query.y[1])
		# 	mean = profile[query.x[1]]
		# else:
		profile = self.problem.profile  # weights for each parameter as usual 
		params.append(query.x)
		params.append(query.y)
		# mean = profile[query.x]
		# r = np.random.normal(mean, self.problem.sd)
		s = math.exp(self.problem.gamma * profile[params[0]]) / \
                    (math.exp(self.problem.gamma * profile[params[0]]) + \
					math.exp(self.problem.gamma * profile[params[1]]))
		# alternative denominator:
		# sum([math.exp(self.problem.gamma * self.problem.profile[params[i]]) 
		# for i in params])
		if s > .5:
			v = 1.
		elif s < .5:
			v = 0.
		self.problem.answer = v

	def answerQuestion(self, query):
		'''
		This query response function encompasses both queries types.
		'''
		if type(query).__name__ in ('BoundQueryFlat', 'BoundQueryLinear'):
			self.answerBound(query)
		elif type(query).__name__ in ('ComparisonQueryFlat', \
			'ComparisonQueryLinear'):
			self.answerComparison(query)

	def computeUtility(self, utility='linear'):
		'''
		This function returns a user's utility for each item.
		utility : str
		This string denotes which utility type the function should 
		implicitly be called for.
		'''
		u = []
		for i in range(self.problem.numItems()):
			v = 0
			if utility =='linear':
				v = reduce(lambda x, y: x + y, (self.problem.evalUtility(
				self.problem.profile, (j, i)) for j in range(self.problem.numParams())))
			else:  # 'flat'
				v = self.problem.evalUtility(self.problem.profile, i)
			u.append(v)
			print('my utility for item ' + str(i) + ': is ' + str(v))
		self.problem.user = [u.index(max(u)), max(u)]
