
# %% [markdown]
# # Calculating expected value using a pdf

# %%
import numpy as np
import scipy.stats as st
import scipy.integrate as itg
import matplotlib.pyplot as plt

plt.style.use('fivethirtyeight')

# %%
x = np.linspace(-2, 1.5, 250)
posterior = st.norm(.5, .25).pdf(x)
plt.plot(x, posterior);

def norm_pdf(self, mu, sigma, x):
    value = st.norm().pdf(x)
    return value

# %% Integrate pdf between x_1 = 0. and x_2 = .70
x_1 = -2
x_2 = 1.2

res, err = itg.quad(norm_pdf, x_1, x_2)
print('Integration bewteen {} and {}: '.format(x_1, x_2), res)

# %% Calculate EV
f = lambda x : x
# there exist a 'conditional' argument which corrects by conditional probability of the integ. interval
expectation_1 = st.norm(.5, .25).expect(f)
expectation_2 = st.norm(loc=par_1, scale=par_2).mean()
print(str(expectation_2))
print("expect_1: " + str(expectation_1) + "expect_2: " + str(expectation_2))

outcomes = ["vw", "renault", "toyota", "ford"]
attributes_add = ["body_type", "engine_size", "fuel_consumption", "drivetrain"]
attributes_gai = [("body_type"), ("engine_size", "fuel_consumption"),
                  ("drivetrain", "fuel_consumption")]
database = {}
database = {k_1: {k_2: None for k_2 in attributes_add} for k_1 in outcomes}
for k, v in database.items():
    print(k, v)

scaling_init = np.ones(len(attributes_add))
print(str(scaling_init))
expectation_prior = np.zeros((len(outcomes), len(attributes_add)))
print(str(expectation_prior))
par_1 = np.full((len(outcomes), len(attributes_add)), .5)
par_2 = np.ones((len(outcomes), len(attributes_add)))

for x in np.nditer(expectation_prior, op_flags=['readwrite']):
        x[...] = st.norm(loc=par_1, scale=par_2).expect(f)

expectation_uniform = np.zeros((len(outcomes), len(attributes_add)))
test = st.uniform.rvs()
print(str(test))

query_type =  ["comp", "g"]
data = ["gt", ["", ""]]

"""
Utilities are stored in the self.full_expectation; self.full_expectation[;,0] provides the
global utility for each outcome.

GAI: In order to create the appropriate array begin with a list of tuples e.g. [(), (), ()].
Enumerate the over the list in lieu of attributes in order to create a corresponding array.
self.elicitor.reference [list] must be provided in order to using the gai model.
"""

class Reasoner():
    def __init__(self):
        self._outcomes = outcomes
        # self._outcomes = self.elicitor.outcomes   
        self._attributes = attributes_add
        # self._attributes = self.elicitor.attributes
        self.scaling_init = np.ones(len(attributes_add))
        # self.scaling_init = np.ones(len(self._attributes))
        self._shape = [len(self._outcomes), len(self._attributes)]
        self.expectation_prior = np.zeros((self._shape))
        self._expectation_uniform = np.zeros((self._shape))
        self.query_mask = ma.make_mask_none((self._shape))

        # set distribution parameters based on type of prior    
        if self.simulator.dist = norm:
            self.par_1 = np.full(
                (len(self._outcomes), len(self._attributes)), .5)
            self.par_2 = np.ones((len(self._outcomes), len(self._attributes)))
        else self.simulator.dist = stu:
            # parameters to be determined

        # calculate a uniform prior for 1st MCMC step
            for x in np.nditer(_expectation_uniform, op_flags=['readwrite']):
                    x[...] = st.uniform.rvs()

        self.expectation_prior = self._expectation_uniform

    def update(self, utility_model=add, dist=norm, sampler=None):  # dist=self.simulator.dist
        """
            utility_model: The type of utility model to be used for updating (add or gai).
            dist: The type of prior pdf that is assumed of each user (norm or stu).
            sampler: The type of sampler to be used for generating the posterior pdf (metropolis or nuts).
        """
        self.response = self.simulator.elicitor.response

        if sampler is not None:
            self.sampler = sampler
        else:   
            self.sampler = self.simulator.sampler.metropolis(self.data)

        # check query_type in order to find the array position(s) of the 
        # relevant attribute(s)/attribute values(s)
        if self.elicitor.query_type[0] = "bound" and self.elicitor.query_type[1] = "l"
            # create array mask using self.elicitor.data
        elif self.elicitor.query_type[0] = "bound" and self.elicitor.query_type[1] = "g"
        
        elif self.elicitor.query_type[0] = "comp" and self.elicitor.query_type[1] = "l"

        elif self.elicitor.query_type[0] = "comp" and self.elicitor.query_type[1] = "g"

        elif self.elicitor.query_type[0] = "rank" and self.elicitor.query_type[1] = "l"

        else self.elicitor.query_type[0] = "rank" and self.elicitor.query_type[1] = "g"

        # update of distribution parameters
        for i in enumerate(self._attributes):
            if query_mask:

            par_1_post[i], par_2_post[i] = self.sampler

        # calculation of self.expectation_post
        self.expectation(dist, self.par_1,
                         self.par_2, self.expectation_post, scaling=None)

        # calculation EVOI
        self.evoi(self.expectation_prior, self.expectation_post)

        # calculation of expected loss  
        self.expected_loss()

        # update of the prior expectation
        self.expectation_prior = self.expectation_post

    def expectation(self, dist=norm, par_1, par_2, output, scaling=None):
        """
        Args:
            dist: The type of distribution that expected values are being calculated for (norm or beta).
            par_1: The mean of the Gaussian distribution/1st parameter for Student t].
            par_2: The standard deviation of the Gaussian distribution/2nd parameter for Student t].
            output: Name of array returned.
        """
        self._cum_expectation = np.zeros((len(self._outcomes, 1)))
        if scaling is not None:
            self.scaling = self.scaling_init
        else:
            self.scaling = scaling

        for i in enumerate(self._outcomes):
            for j in enumerate(self._attributes):
            if dist = norm:
                output[i, j] = st.norm(par_1[k], par_2[k]).expect()
            else:
                output[i, j] = st.t().expect()
        
        # apply the scaling variable to the expectation matrix

        return output

    def evoi():



#%%
