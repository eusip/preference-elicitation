# standard libraries
from math import sqrt  # latexify
import operator
from functools import reduce
import heapq

# third-party libraries
import matplotlib  # latexify
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


class Problem:
	'''
	This is the core class of the simulator. It defines a problem.
	'''
	def __init__(self, items, params):
		self.items = 0
		self.params = 0
		self.noise = .05  # constant noise value
		self.gamma = 25.0  # softmax temperature parameter
		self.queries = {}
		self.question = None
		self.answer = 0.0
		self.eu = []
		self.preferences = []
		self.metropolis = []  # the metropolis sampler walk
		self.importance = []  # the importance sampler walk
		self.mean = .5  # normal distribution initial mean weight for 
		# each feature for both metropolis.computeSample() and 
		# user.generateProfile()
		self.sd = .5  # standard deviation for feature weights
		self.recommendation = [] # a list containing the max EU after 
		# each question
		# self.loss = []  # a list containing the loss after each query
		self.profile = None
		self.user = []  # the item index and utility of the most 
		# preferred item of the user
		self.cycle = 0  # cycle counter for computeUtilities()
		# self.history = []
		# np.random.seed(555)
		# np.random.seed(404)
		# np.random.seed(123)

	def numItems(self):
		'''
		This function returns the number of items in the problem.
		'''
		return self.items

	def updatePrefs(self):
		'''
		This function adds a new query-response dictionary pair to the 
		list of preferences.
		'''
		print('the question is: ' + str(self.question))
		print('the answer is: ' + str(self.answer))
		self.preferences.append(
			{'query': self.question, 'response': self.answer})

	def printPrefs(self):
		'''
		This function prints the list of currently recorded preferences.
		'''
		for pref in self.preferences:
			print(str(pref['query']) + " --> " + str(pref['response']))

	def printWalk(self, sampler='metropolis'):
		if sampler == 'metropolis':
			# m = (reduce(lambda x, y: x + y, [self.metropolis[i] for i in \
			# 	range(len(self.metropolis))]) / len(self.metropolis))[0]
			# print('The means weights at this point are: ' + str(m))
			print(str(self.metropolis))
		else:  # importance
			print(str(self.importance))

	def printUtilities(self):
		'''
		This function prints out the expected utility of each item given
		 the query responses currently provided.
		'''
		for item in range(len(self.eu)):
			print('the expected utility of item ' + str(item) + ': ' + \
				str(self.eu[item]))

	def printNQueries(self, n):
		'''
		This function prints the top n queries proposed based on the 
		maximization of EUS.
		Parameters
		----------
		n : int
		The number of queries printed out.
		'''
		top_n = heapq.nlargest(n, self.queries.keys(), key=self.queries.get)
		for query in top_n:
			print(str(query) + ': ' + str(self.queries.get(query)))

	def returnFeatures(self, utility):
		'''
		This function returns the boolean feature values for each item. 
		The columns of the array represent alternatives while the rows 
		represent features.
		Parameters
		----------
		utility : str
		This string denotes which utility type the function should 
		implicitly be called for. 
		Returns
		-------
		v : int
		The feature array for the selected item.
		'''
		# np.random.seed(213) # 6, 12 items
		np.random.seed(504) # 12 items
		# np.random.seed(821) # 24 items

		if utility == 'flat':
			v = np.array([1, 1, 1, 1, 1, 1, 1, 1])
		else:
			# 4 items, 4 features
			# v = np.array([[1, 0, 1, 1,], [0, 1, 0, 0], [1, 1, 0, 1], 
			# [0, 1, 1, 1]])
			# 6 items, 8 features
			# v = np.array([[1, 0, 1, 1, 0, 1], [0, 1, 0, 0, 1 ,1], [1, 1, 0, 1, 1, 0], \
			# 	[0, 1, 0, 1, 1, 0], [1, 1, 1, 0, 1, 0], [0, 0, 1, 1, 1, 0],
			# 	[1, 0, 1, 0, 1, 0], [0, 1, 0, 1, 0, 1]])
			# 12 items, 16 features
			v = np.random.randint(2, size=(16, 16))  # features, items
		return v

	def evalUtility(self, w, param, utility):
		'''
		This function evaluates the utility of a feature 
		(globally or locally) given a user's feature weights and an
		 item's feature values.
		Parameters
		----------
		w : array (NumPy)
		This is the array of user weights.
		param : int
		This is the feature being evaluated.
		utility : str
		This string denotes which utility type the function should 
		implicitly be called for.
		Returns
		-------
		v : int
		The utility of the selected item.
		'''
		v = 0
		if isinstance(param, int):
			if utility == 'flat':
				v = np.multiply(w[param], self.returnFeatures(
					utility=utility)[param])
			else:  # linear
				v = np.sum(np.multiply(w[param], self.returnFeatures(
					utility=utility)[param,:]))
		elif isinstance(param, tuple):
			v = (np.multiply(w[param[0]],	self.returnFeatures(
				utility=utility)[param[0], param[1]]))
		elif isinstance(param, float):  # utility level  
			v = param
		return v

	def computeUtilities(self, utility, sampler, reset):
		'''
		This function computes the expected utilities of each item 
		based on the samples generated by the metropolis sampler or the 
		last importance resampling. It stores the highest item utility 
		in self.recommendation.
		utility : str
		This string denotes which utility type the function should 
		implicitly be called for.
		sampler : str
		This string denotes which set of samples should be used in 
		computing expected utility.
		reset : str
		This string resets the cycle variable at the beginning of each 
		new elicitation cycle.
		'''
		if reset == 'y':
			self.cycle = 0
		self.eu.clear()
		if sampler == 'metropolis':
			samples = self.metropolis
		else:  # importance
			samples = self.importance
		w = reduce(lambda x, y: x + y, [samples[i] for i in \
			range(len(samples))]) / len(samples)
		for i in range(self.numItems()):
			v = 0
			if utility == 'linear':
				v = reduce(lambda x, y: x + y, [self.evalUtility(
				w, (j, i)) for j in range(self.numParams())])
				self.eu.append(v)
			else:  # 'flat'
				self.eu.append(self.evalUtility(w, i))
		self.recommendation.append((self.cycle, self.eu.index(max(self.eu)), 
									max(self.eu)))
		self.cycle += 1
		print('the recommendation list is: ' + str(self.recommendation)) 
		
	def computeLoss(self, q_type, runs, utility):
		'''
		This function computes the difference in utility between a 
		user's utility for their preferred item and the maximum expected
		 utility amongst the alternatives available.
		Parameters
		----------
		q_type : str
		The type of query being considered. EPU - queries are selected 
		based on maximum expected posterior utility; bound - random 
		bound queries are selected; comparison - random comparison 
		queries are selected; mixture - an alternation of bound and
		comparison queries are selected.
		'''
		loss = []
		# print('user: ' + str(self.user))
		# print('recommendation: ' + str(self.recommendation))
		cycles = max(self.recommendation)[0]
		# print('the variable cycles is: ' + str(cycles))
		if utility == 'flat':
			for i in range(cycles + 1):
				l = 0.0
				for j in range(len(self.recommendation)):
					# print('the cycle is: ' + str(i))
					if self.recommendation[j][0] == i:
						# print('the item utility is: ' + str(self.recommendation[j][2]))
						l += self.user[1] - self.profile[self.recommendation[j][1]]
						# print('the cumulative loss is: ' + str(l))
				loss_ = l/runs
				# print('the mean loss for 3 cycles is: ' + str(loss_))
				loss.append(loss_)
		else:  # 'linear'
			for i in range(cycles + 1):
				u = 0.0
				l = 0.0
				for j in range(len(self.recommendation)):
					if self.recommendation[j][0] == i:
						u = reduce(lambda x, y: x + y, [self.evalUtility(self.profile,
                                    (k, self.recommendation[j][1])) for k in
                                    range(self.numParams())])
						l += self.user[1] - u
				loss_ = l/runs
				loss.append(loss_)
		if q_type == 'EPU':
			# print('the EPU loss is: ' + str(loss))
			self.EPUloss = np.asarray(loss)
		elif q_type == 'bound':
			# print('the bound loss is: ' + str(loss))
			self.boundloss = np.asarray(loss)
		elif q_type == 'comparison':
			# print('the comparison loss is: ' + str(loss))
			self.comparisonloss = np.asarray(loss)
		elif q_type == 'mixture':
			# print('the mixture loss is: ' + str(loss))
			self.mixtureloss = np.asarray(loss)
		self.recommendation.clear()

	def computeEPULoss(self, q_type, runs, utility, comparison):
		'''
		This function computes the difference in utility between a 
		user's utility for their preferred item and the maximum expected
		 utility amongst the alternatives available. In this setting all
		 queries are selected based on maximum expected posterior utility.
		Parameters
		----------
		q_type : str
		The type of query being considered. bound - only bound queries 
		are considered; comparison - only comparison are considered; 
		mixture - both query types are considered
		runs : str
		The number of runs being considered.
		'''
		loss = []
		# print('user: ' + str(self.user))
		# print('recommendation: ' + str(self.recommendation))
		cycles = max(self.recommendation)[0]
		# print('the variable cycles is: ' + str(cycles))
		if utility == 'flat': 
			for i in range(cycles + 1):
				l = 0.0
				for j in range(len(self.recommendation)):
					# print('the cycle is: ' + str(i))
					if self.recommendation[j][0] == i:
						# print('the item utility is: ' + str(self.recommendation[j][2]))
						l += self.user[1] - self.profile[self.recommendation[j][1]]
						# print('the cumulative loss is: ' + str(l))
				loss_ = l/runs
				# print('the mean loss for 3 cycles is: ' + str(loss_))
				loss.append(loss_)
		else:  # 'linear'
			for i in range(cycles + 1):
				u = 0.0
				l = 0.0
				for j in range(len(self.recommendation)):
					if self.recommendation[j][0] == i:
						u = reduce(lambda x, y: x + y, [self.evalUtility(self.profile,
									(k, self.recommendation[j][1])) for k in \
									range(self.numParams())])
						l += self.user[1] - u
				loss_ = l/runs
				loss.append(loss_)
		if q_type == 'bound':
			# print('the bound loss is: ' + str(loss))
			self.boundloss = np.asarray(loss)
		elif q_type == 'comparison':
			# print('the comparison loss is: ' + str(loss))
			self.comparisonloss = np.asarray(loss)
		elif q_type == 'mixture':
			if comparison == 'all':
				# print('the EPU loss is: ' + str(loss))
				self.EPUloss = np.asarray(loss)
			else:  # 'EPU'
				# print('the mixture loss is: ' + str(loss))
				self.mixtureloss = np.asarray(loss)
		self.recommendation.clear()

class FlatUtilityProblem(Problem):
	'''
	Parameters
	----------
	items : int
	The number of items being considered.
	params : int
	The number of features characterizing each item. This value is 
	necessarily 1.
	'''
	def __init__(self, items, params):
		super().__init__(items, params)
		self.items = items
		self.eu = [None] * self.numItems()  # the expected utility of
		# each item

	def returnFeatures(self, utility='flat'):
		return super().returnFeatures(utility)
	
	def evalUtility(self, w, item, utility='flat'):
		return super().evalUtility(w, item, utility)

	def computeUtilities(self, utility='flat', sampler='metropolis', reset='n'):
		return super().computeUtilities(utility, sampler, reset)

	def computeLoss(self, q_type, runs, utility='flat'):
		return super().computeLoss(q_type, runs, utility)

	def computeEPULoss(self, q_type, runs, utility='flat', comparison='EPU'):
		return super().computeEPULoss(q_type, runs, utility, comparison)



class LinearUtilityProblem(Problem):
	'''
	Parameters
	----------
	items : int 
	The number of items being considered.
	params : int
	The number of features characterizing each item.
	'''
	def __init__(self, items, params):
		super().__init__(items, params)
		self.items = items
		self.params = params
		self.eu = [None] * self.numItems()  # the expected utility of
		# each item

	def numParams(self):
		'''
		This function returns the number of features characterizing 
		each item in this problem.
		'''
		return self.params

	def returnFeatures(self, utility='linear'):
		return super().returnFeatures(utility)

	def evalUtility(self, w, item, utility='linear'):
		return super().evalUtility(w, item, utility)
	
	def computeUtilities(self, utility='linear', sampler='metropolis', reset='n'):
	 	return super().computeUtilities(utility, sampler, reset)

	def computeLoss(self, q_type, runs, utility='linear'):
		return super().computeLoss(q_type, runs, utility)

	def computeEPULoss(self, q_type, runs, utility='linear', comparison='EPU'):
		return super().computeEPULoss(q_type, runs, utility, comparison)

class Chart():
	'''
	p : obj
	The problem instance.
	'''
	problem = None

	def __init__(self, p):
		self.problem = p

	def plotLoss(self):
		self.cycles = np.arange(len(self.problem.comparisonloss))

		fig_width = 8.0 # 6.9
		golden_mean = (sqrt(5)-1.0)/2.0		
		fig_height = fig_width*golden_mean
		
		params = {'backend': 'ps',
					'text.latex.preamble': ['\\usepackage{gensymb}'],
					# fontsize for x and y labels (was 10) previously 8
					'axes.labelsize': 10,
					'axes.titlesize': 10,
					'font.size': 10,  # was 10
					'legend.fontsize': 10,  # was 10
					'xtick.labelsize': 10,
					'ytick.labelsize': 10,
					'text.usetex': True,
					'figure.figsize': [fig_width, fig_height],
					'font.family': 'serif'
			}

		matplotlib.rcParams.update(params)

		fig, ax = plt.subplots()
		ax.plot(self.cycles, self.problem.EPUloss,
					label='Mixture (EPU)', color='red', linestyle='solid', marker='o')
		ax.plot(self.cycles, self.problem.boundloss,
				 label='Bound (random)', color='blue', linestyle='dashed', marker='o')
		ax.plot(self.cycles, self.problem.comparisonloss,
					label='Comparison (random)', color='green', linestyle='dashdot', \
						marker='o')
		# ax.plot(self.cycles, self.problem.mixtureloss,
		# 		 label='Mixture', color='purple', linestyle='dotted', \
		# 			 marker='o')
		ax.set(xlabel='recommendation cycles', ylabel='utility loss') #, 
			# title='All strategies, 24 items, 18 features, Normal dist. ' + '(gamma = ' + \
				# str(self.problem.gamma) + ', 1 run)')
		ax.legend()
		ax.grid()
		plt.tight_layout()
		fig.savefig('all_m_l_n_i16_f16_q10_g25_s6k.png')
		# fig.savefig('all_m_l_n_i6_q15_g25_r1_s404.png')
		plt.show()

	def plotEPULoss(self):
		self.cycles = np.arange(len(self.problem.comparisonloss))

		fig_width = 8.0  # 6.9
		golden_mean = (sqrt(5)-1.0)/2.0
		fig_height = fig_width*golden_mean

		params = {'backend': 'ps',
					'text.latex.preamble': ['\\usepackage{gensymb}'],
					# fontsize for x and y labels (was 10) previously 8
					'axes.labelsize': 10,
					'axes.titlesize': 10,
					'font.size': 10,  # was 10
					'legend.fontsize': 10,  # was 10
					'xtick.labelsize': 10,
					'ytick.labelsize': 10,
					'text.usetex': True,
					'figure.figsize': [fig_width, fig_height],
					'font.family': 'serif'
			}

		matplotlib.rcParams.update(params)

		fig, ax = plt.subplots()
		ax.plot(self.cycles, self.problem.boundloss,
					label='Bound (EPU)', color='red', linestyle='dashed', marker='o')
		ax.plot(self.cycles, self.problem.comparisonloss,
					label='Comparison (EPU)', color='blue', linestyle='dashdot', \
						marker='o')
		ax.plot(self.cycles, self.problem.mixtureloss,
					label='Mixture (EPU)', color='green', linestyle='dotted', \
						marker='o')
		ax.set(xlabel='recommendation cycles', ylabel='utility loss') #, 
			# title= 'EPU-only, 6 items,  8 features, Normal dist. ' + '(gamma = ' + \
				# str(self.problem.gamma) + ', 1 run)')
		ax.legend()
		ax.grid()
		plt.tight_layout()
		fig.savefig('epu_m_l_n_i12_f16_q10_g25_s12k.png')
		# fig.savefig('epu_m_f_u_i6_q15_g25_r1_s404.png')
		plt.show()

	#Original Author: Prof. Nipun Batra
	# nipunbatra.github.io
	SPINE_COLOR = 'gray'

	def latexify(fig_width=None, fig_height=None, columns=1):
		'''
		Set up matplotlib's RC params for LaTeX plotting.
		Call this before plotting a figure.
		Parameters
		----------
		fig_width : float, optional, inches
		fig_height : float,  optional, inches
		columns : {1, 2}
		'''

		# code adapted from 
		# http://www.scipy.org/Cookbook/Matplotlib/LaTeX_Examples

		# Width and max height in inches for IEEE journals taken from
		# computer.org/cms/Computer.org/Journal%20templates/ \
		# transactions_art_guide.pdf

	
		assert(columns in [1, 2])

		if fig_width is None:
			if columns == 1:
				fig_width = 3.39  
			else:
				6.9  # width in inches

		if fig_height is None:
			golden_mean = (sqrt(5)-1.0)/2.0    # Aesthetic ratio
			print('fig width' + str(fig_width))
			print('golden mean' + str(golden_mean))
			fig_height = fig_width*golden_mean  # height in inches

		MAX_HEIGHT_INCHES = 8.0
		if fig_height > MAX_HEIGHT_INCHES:
			print("WARNING: fig_height too large:" + fig_height +
				  "so will reduce to" + MAX_HEIGHT_INCHES + "inches.")
			fig_height = MAX_HEIGHT_INCHES

		params = {'backend': 'ps',
				  'text.latex.preamble': ['\\usepackage{gensymb}'],
				  'axes.labelsize': 8,  # fontsize for x and y labels (was 10)
				  'axes.titlesize': 8,
				  'font.size': 8,  # was 10
				  'legend.fontsize': 8,  # was 10
				  'xtick.labelsize': 8,
				  'ytick.labelsize': 8,
				  'text.usetex': True,
				  'figure.figsize': [fig_width, fig_height],
				  'font.family': 'serif'
				  }

		matplotlib.rcParams.update(params)
	
	def format_axes(ax):

		for spine in ['top', 'right']:
			ax.spines[spine].set_visible(False)

		for spine in ['left', 'bottom']:
			ax.spines[spine].set_color(SPINE_COLOR)
			ax.spines[spine].set_linewidth(0.5)

		ax.xaxis.set_ticks_position('bottom')
		ax.yaxis.set_ticks_position('left')

		for axis in [ax.xaxis, ax.yaxis]:
			axis.set_tick_params(direction='out', color=SPINE_COLOR)

		return ax
