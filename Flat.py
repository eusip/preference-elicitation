# %% [markdown]
# # Flat Utility Problem

# This is a working deployment of the flat utility problem.

# %% [markdown]
#Load the following modules to run the flat utility problem.
import numpy as np

from Problem import FlatUtilityProblem
from Query import BoundQueryFlat, ComparisonQueryFlat
from Reasoner import Reasoner
from Sampler import MetropolisFlat

# %% [markdown]
# For analysis seed the random generator with a constant value.
np.random.seed(504)

# %% [markdown]
# Instantiate a flat utility problem as well as two questions. Then
# instantiate a reasoner and the sampler.
 
p = FlatUtilityProblem(4)
q1 = BoundQueryFlat(p, [0, 0, 1, 1], 1.5)
q2 = ComparisonQueryFlat(p, 1, 0)
r = Reasoner(p)
s = MetropolisFlat(p, r)

# %% [markdown]
# A response is provided as well as a random walk for query 1 and query 2.
r.addPref(q1, 1.)
r.printPrefs()
s.takeWalk()
print(str(s.walk))
r.addPref(q2, 0.)
r.printPrefs()
s.takeWalk()
print(str(s.walk))
s.evalUtility()