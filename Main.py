# standard libraries
import math
import time

# local libraries
from Problem import FlatUtilityProblem, LinearUtilityProblem
from Problem import Chart
from Query import BoundQueryFlat, ComparisonQueryFlat
from Query import BoundQueryLinear, ComparisonQueryLinear
from Query import Engine
from Reasoner import ReasonerFlat, ReasonerLinear
from Sampler import MetropolisFlat, MetropolisLinear
from User import User
from Elicitation import Elicitation 

def main():
    # note: there are only 6 comparison queries for a 4 item problem
    utility = 'linear'  # 'linear', 'flat'
    comparison = 'all'  # 'all', 'EPU'
    # alternatives =
    # features = 
    queries = 10 #10 queries max!
    warmup = 1200
    samples = 6000 #6k max!
    runs = 1
    sampler = 'metropolis'  # 'importance', 'hybrid'
    response = 'logistic'  # 'constant'
    dist = 'normal'  # 'normal'

    if utility =='linear':
        # linear problem
        p = LinearUtilityProblem(16, 16)
        q = Engine(p)
        r = ReasonerLinear(p)
        s = MetropolisLinear(p)
        u = User(p)
        c = Chart(p)
        print('the feature array is: ' + str(p.returnFeatures()))
        if comparison == 'all':
            # analysis of all query orderings
            for run in range(runs):
                u.generateProfile(utility='linear')
                for q_type in ['bound', 'comparison', 'mixture']:
                    print(str(q_type) + ' Query')
                    if q_type in ('bound','comparison'):
                        Elicitation.elicitLinear(p, q, r, s, u, queries, warmup, 
                                                samples, sampler=sampler, 
                                                q_type=q_type)
                        p.computeLoss(q_type=q_type, runs=runs)
                    elif q_type == 'mixture':
                        Elicitation.elicitEPULinear(p, q, r, s, u, queries, warmup,
                                                  samples, sampler=sampler, q_type='mixture')
                        p.computeEPULoss(q_type=q_type, runs=runs, comparison='all')
            # title = 
            # fname = 
            c.plotLoss()  # fname
        else:  # EPU
            # analysis of only EPU query orderings
            for run in range(runs):
                u.generateProfile(utility='linear')
                for q_type in ['bound', 'comparison', 'mixture']:
                    print(str(q_type) + ' Query')
                    Elicitation.elicitEPULinear(p, q, r, s, u, queries, warmup,
                                                    samples, sampler=sampler,
                                                    q_type=q_type)
                    p.computeEPULoss(q_type=q_type, runs=runs)
            # title = 
            # fname = 
            c.plotEPULoss()  # fname
    else:  # 'flat'
        # flat problem
        p = FlatUtilityProblem(8, 1)
        q = Engine(p)
        r = ReasonerFlat(p)
        s = MetropolisFlat(p)
        u = User(p)
        c = Chart(p)
        print('the feature array is: ' + str(p.returnFeatures()))
        if comparison == 'all':
            # analysis of all query orderings
            for run in range(runs):
                u.generateProfile(utility='flat')
                for q_type in ['bound', 'comparison', 'mixture']:
                    print(str(q_type) + ' Query')
                    Elicitation.elicitFlat(p, q, r, s, u, queries, warmup,
                                            samples, sampler=sampler, 
                                            q_type=q_type)
                    p.computeLoss(q_type=q_type, runs=runs)
                print('EPU Query')
                for run in range(runs):
                    Elicitation.elicitEPUFlat(p, q, r, s, u, queries, warmup,
                                            samples, sampler=sampler, q_type='mixture')
                    p.computeEPULoss(q_type='mixture', runs=runs, comparison='all')
            # title = 
            # fname = 
            c.plotLoss()  # fname
        else:  # EPU
            # analysis of only EPU query orderings
            for run in range(runs):
                u.generateProfile(utility='flat')
                for q_type in ['bound', 'comparison', 'mixture']:
                    print(str(q_type) + ' Query')
                    Elicitation.elicitEPUFlat(p, q, r, s, u, queries, warmup,
                                                samples, sampler=sampler, q_type=q_type)
                    p.computeEPULoss(q_type=q_type, runs=runs)
            # title = 
            # fname = 
            c.plotEPULoss()  # fname

    # print('EPU QUERY')
    # Elicitation.elicitLinear(p, q, r, s, u, 6, 500, 2500,
    #                         sampler='metropolis', q_type='EPU')
    # p.computeLoss(q_type='EPU')
    # print('BOUND QUERY')
    # Elicitation.elicitLinear(p, q, r, s, u, 6, 500, 2500,
    #                         sampler='metropolis', q_type='bound')
    # p.computeLoss(q_type='bound')
    # print('COMPARISON QUERY')
    # Elicitation.elicitLinear(p, q, r, s, u, 6, 500, 2500,
    #                         sampler='metropolis', q_type='comparison')
    # p.computeLoss(q_type='comparison')
    # print('MIXTURE QUERY')
    # Elicitation.elicitLinear(p, q, r, s, u, 6, 500, 2500,
    #                         sampler='metropolis', q_type='mixture')
    # p.computeLoss(q_type='mixture')

    # c.plotLoss()
    
    # analysis of only EPU query orderings
    # u.generateProfile(utility='flat')
    # print('BOUND QUERY')
    # Elicitation.elicitEPULinear(p, q, r, s, u, 6, 5000, 20000,
    #                          sampler='metropolis', q_type='bound')
    # p.computeEPULoss(q_type='bound')
    # print('COMPARISON QUERY')
    # Elicitation.elicitEPULinear(p, q, r, s, u, 6, 5000, 20000,
    #                          sampler='metropolis', q_type='comparison')
    # p.computeEPULoss(q_type='comparison')
    # print('MIXTURE QUERY')
    # Elicitation.elicitEPULinear(p, q, r, s, u, 6, 5000, 20000,
    #                          sampler='metropolis', q_type='mixture')
    # p.computeEPULoss(q_type='mixture')
    
    # c.plotEPULoss()

    
    # analysis of all query orderings
    # print('EPU QUERY')
    # Elicitation.elicitFlat(p, q, r, s, u, 3, 1500, 10000,
    #                         sampler='metropolis', q_type='EPU')
    # p.computeLoss(q_type='EPU')
    # print('BOUND QUERY')
    # Elicitation.elicitFlat(p, q, r, s, u, 3, 1500, 10000,
    #                         sampler='metropolis', q_type='bound')
    # p.computeLoss(q_type='bound')
    # print('COMPARISON QUERY')
    # Elicitation.elicitFlat(p, q, r, s, u, 3, 1500, 10000,
    #                         sampler='metropolis', q_type='comparison')
    # p.computeLoss(q_type='comparison')
    # print('MIXTURE QUERY')
    # Elicitation.elicitFlat(p, q, r, s, u, 3, 1500, 10000,
    #                         sampler='metropolis', q_type='mixture')
    # p.computeLoss(q_type='mixture')

    # c.plotLoss()

    # analysis of only EPU query orderings
    # u.generateProfile(utility='flat')
    # print('BOUND QUERY')
    # Elicitation.elicitEPUFlat(p, q, r, s, u, 3, 5000, 20000,
    #                             sampler='metropolis', q_type='bound')
    # p.computeEPULoss(q_type='bound')
    # print('COMPARISON QUERY')
    # Elicitation.elicitEPUFlat(p, q, r, s, u, 3, 5000, 20000,
    #                             sampler='metropolis', q_type='comparison')
    # p.computeEPULoss(q_type='comparison')
    # print('MIXTURE QUERY')
    # Elicitation.elicitEPUFlat(p, q, r, s, u, 3, 5000, 20000,
    #                             sampler='metropolis', q_type='mixture')
    # p.computeEPULoss(q_type='mixture')

    # c.plotEPULoss()

if __name__ == '__main__':
	start = time.time()
	main()
	end = time.time()
	print("Execution time: " + str(end-start) + " seconds")
