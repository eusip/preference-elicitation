# -*- coding: utf-8 -*-

from scipy.stats import norm
from copy import copy

'''
In order to calculate the expectation using the pdf using the returned values from metropolis,
 use numpy.trapz in order to implement the composite trapezoidal rule.

To implement: prior_type argument
'''

class Samplers():
    def metropolis(self, data, prior_type=norm, samples=1000, mu_init=.5, sigma_proposal=.5, mu_prior=0, sigma_prior=1., plot=False):
        # mu_init: walk starting point, sigma_proposal: walk step size
        mu_current = mu_init
        # initialize the posterior list with the value mu_init for [0]
        posterior = [mu_current]
        # calculate the random walk
        for i in range(samples):
            # suggest new position; these distribution characteristics have 
            # NOTHING to do with the model distribution they simply 
            # characterize the random walk
            mu_proposal = norm(mu_current, sigma_proposal).rvs()

            # compute likelihood; does not have to be normal
            likelihood_current = norm(mu_current, 1).pdf(data).prod()
            likelihood_proposal = norm(mu_proposal, 1).pdf(data).prod()

            # compute prior 
            prior_current = norm(mu_prior, sigma_prior).pdf(mu_current)
            prior_proposal = norm(mu_prior, sigma_prior).pdf(mu_proposal)

            # Bayes' formula
            post_current = likelihood_current * prior_current
            post_proposal = likelihood_proposal * prior_proposal

            # accept proposal?; we are visiting regions of high post. prob. more than regions of \
            # low post. prob.
            post_accept = post_proposal / post_current

            # because prior_current and prior_proposal drawn from the same symmetric distribution
            accept = np.random.rand() < post_accept 

            if plot and i == (samples - 1):
                self.plot_results(self, mu_current, mu_proposal, mu_prior, sigma_prior, data, accept, posterior, i)

            # update position of mu
            if accept:
                mu_current = mu_proposal

            # append random walk
            posterior.append(mu_current)

        walk = np.array(posterior)
        print("minimum mu: " + str(walk.min()), "maximum mu: " + str(walk.max()),
              "mean mu: " + str(walk.mean()), "last mu: " + str(walk[i]))
        return walk[i], sigma_prior

    def calc_posterior_analytical(self, data, x, mu_0, sigma_0):
        sigma = 1.
        n = len(data)
        mu_post = (mu_0 / sigma_0**2 + data.sum() / sigma**2) / \
            (1. / sigma_0**2 + n / sigma**2)
        sigma_post = (1. / sigma_0**2 + n / sigma**2)**-1
        return norm(mu_post, np.sqrt(sigma_post)).pdf(x)

    def plot_results(self, mu_current, mu_proposal, mu_prior, sigma_prior, data, accepted, trace, i):
        trace = copy(trace)
        fig, (ax1, ax2, ax3, ax4) = plt.subplots(ncols=4, figsize=(16, 4))
        fig.suptitle('Iteration %i' % (i + 1))
        x = np.linspace(0, 1, 100)
        color = 'g' if accepted else 'r'

        # plot prior density
        prior_current = norm(mu_prior, sigma_prior).pdf(mu_current)
        prior_proposal = norm(mu_prior, sigma_prior).pdf(mu_proposal)
        prior = norm(mu_prior, sigma_prior).pdf(x)
        ax1.plot(x, prior)
        ax1.plot([mu_current] * 2, [0, prior_current], marker='o', color='b')
        ax1.plot([mu_proposal] * 2, [0, prior_proposal],
                 marker='o', color=color)
        ax1.annotate("", xy=(mu_proposal, 0.2), xytext=(mu_current, 0.2),
                     arrowprops=dict(arrowstyle="->", color='black', lw=2.))
        ax1.set(xlabel='$\\mu$', ylabel='Probability Density', title='current: prior($\\mu$=%.2f) = %.2f\n  proposal: prior($\\mu$=%.2f) = %.2f' %
                (mu_current, prior_current, mu_proposal, prior_proposal))

        # plot likelihood density
        likelihood_current = norm(mu_current, 1).pdf(data).prod()
        likelihood_proposal = norm(mu_proposal, 1).pdf(data).prod()
        y = norm(loc=mu_proposal, scale=1).pdf(x)
        sns.distplot(data, kde=False, norm_hist=True, ax=ax2)
        ax2.plot(x, y, color=color)
        ax2.axvline(mu_current, color='b', linestyle='--', label='mu_current')
        ax2.axvline(mu_proposal, color=color,
                    linestyle='--', label='mu_proposal')
        ax2.annotate("", xy=(mu_proposal, 0.2), xytext=(mu_current, 0.2),
                     arrowprops=dict(arrowstyle="->", color='black', lw=2.))
        ax2.set(title='likelihood($\\mu$=%.2f) = %.2f\nlikelihood($\\mu$=%.2f) = %.2f' %
                (mu_current, 1e5*likelihood_current, mu_proposal, 1e5*likelihood_proposal))

        # plot posterior density
        posterior_analytical = self.calc_posterior_analytical(
            data, x, mu_prior, sigma_prior)
        ax3.plot(x, posterior_analytical)
        posterior_current = self.calc_posterior_analytical(
            data, mu_current, mu_prior, sigma_prior)
        posterior_proposal = self.calc_posterior_analytical(
            data, mu_proposal, mu_prior, sigma_prior)
        ax3.plot([mu_current] * 2, [0, posterior_current],
                 marker='o', color='b')
        ax3.plot([mu_proposal] * 2, [0, posterior_proposal],
                 marker='o', color=color)
        ax3.annotate("", xy=(mu_proposal, 0.2), xytext=(mu_current, 0.2),
                     arrowprops=dict(arrowstyle="->", color='black', lw=2.))
        ax3.set(title='posterior($\\mu$=%.2f) = %.5f\nposterior($\\mu$=%.2f) = %.5f' %
                (mu_current, posterior_current, mu_proposal, posterior_proposal))

        if accepted:
            trace.append(mu_proposal)
        else:
            trace.append(mu_current)
        ax4.plot(trace)
        ax4.set(xlabel='iteration', ylabel='$\\mu$', title='trace')
        plt.tight_layout()
