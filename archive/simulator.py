# -*- coding: utf-8 -*-

import numpy as np
import scipy.stats as st
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import time

class Simulator():
    def __init__(self, user=None, elicitor=None, query_engine=None, reasoner=None, sampler=None):
        if user is None:
            self.user = User()
        else:
            self.user = user
        if elicitor is None:
            self.elicitor = Elicitor()
        else:
            self.elicitor = elicitor
        if query_engine is None:
            self.query_engine = Query_engine()
        else:
            self.query_engine = query_engine
        if reasoner is None:
            self.reasoner = Reasoner()
        else:
            self.reasoner = reasoner
        if sampler is None:
            self.sampler = Sampler()
        else:
            self.sampler = sampler  
        
        self.outcomes = []
        self.attributes = []
        self.factors = []

    def start_simulation(outcomes=[], attributes=[]):
        self.outcomes = outcomes

# class Visualization():

