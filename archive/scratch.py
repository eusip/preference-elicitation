# %% [markdown]
# # Metropolis random walk

# %%
from Problem import FlatUtilityProblem
from Query import BoundQuery, ComparisonQuery
import Reasoner
import numpy as np
import scipy.stats as st
# import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import time
from scipy.stats import norm
from copy import copy

%matplotlib inline

plt.style.use('fivethirtyeight')
sns.set_style('white')
sns.set_context('notebook')

np.random.seed(123)

# %%

"""
samples: The number of samples to be generated.
size: The step size to each proposed sample.
***add-in a warmup functionality***
"""
p = FlatUtilityProblem(3)
q1 = BoundQuery(p,[1, 0, 1], 2) # is bundle 1/2/3 worth more than utility of 2 out of 10 ?
q2 = ComparisonQuery(p,2, 1) # is item 2 worth more than item 1 ?

print(type(p).__name__)

preferences = []

class Test():
    def addPref(self, q, r):
        preferences.append({"query":q,"response":r})
    
    def computePrior():
        v = np.random.uniform()
        return v


t = Test()
t.addPref(q1, 1)
t.addPref(q2, 0)
print(str(preferences))
print(preferences[1].keys())
print(preferences[0]['query'])
print(preferences[0]['response'])
print(preferences[0]['query'].l)
print(type(preferences[1]['query']))
print(q1.x)
print(q1.l)
np.size(preferences)

if type(preferences[1]['query']) is Query.ComparisonQuery:
    print(str("yes"))

z = np.exp(.01)/(np.exp(.01)*np.exp(.001))
print(str(z))
a = q2.y
print(str(a))

b = t.computePrior
print(str(b))

shape = (1, 4)
s = np.full(shape,.5)
print(str(s))

x_array = np.zeros(shape)
print(str(x_array))
x_array[0,1] = 1

class Metropolis():
    walk = []
    def __init__(self):
        self.size = .2
        self.s = .5

    def computeStep(self,s,size):
        p = norm(s,size).rvs()
        return p

    def computeLikelihood(invert=False):
        if type(preferences[len(preferences)]['query']) is Query.RankQuery:
            pass
        else:
            if not invert:
                v = preferences[len(preferences)]['response']
            else:
                if preferences[len(preferences)]['response'] = 1:
                    v = 0
                elif preferences[len(preferences)]['response'] = 0:
                    v = 1
        return v

    def computePrior():
        v = np.random.uniform()
        return v

    def computeAccept(response=noiseless):
        if response = noiseless:
            a = np.random.rand() < (computeLikelihood()*computePrior() /
                                    computeLikelihood()*self.s)
        elif response = constant:
            a = .95*(np.random.rand() < (computeLikelihood()*computePrior() /
                                         computeLikelihood()*self.s)) +
            .5*(np.random.rand() > (computeLikelihood(True)*computePrior() /
                                     computeLikelihood(True)*self.s))
        elif response = logistic:
            pass # a = np.exp()
        return a

    def takeStep():
        if computeAccept:
            self.s = computeAccept
            walk.append(computeAccept)


class MetropolisFlat(Metropolis):


class MetropolisLinear(Metropolis):
    def computeLikelihood():
        pass

    def computePrior():
        pass

def main():
    s = MetropolisFlat()
    s.computeStep(self.s,self.size)
    s.computeAccept()
    s.takeStep()


"""
start = time.time()
s =
end = time.time()
print("Execution time: " + str(end-start) + " seconds")
"""


#%%
