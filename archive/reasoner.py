# -*- coding: utf-8 -*-

class Reasoner():

    
    def __init__(self):
        self._outcomes = outcomes
        # self._outcomes = self.elicitor.outcomes
        self._attributes = attributes_add
        # self._attributes = self.elicitor.attributes
        self.scaling_init = np.ones(len(attributes_add))
        # self.scaling_init = np.ones(len(self._attributes))
        self._shape = [len(self._outcomes), len(self._attributes)]
        self.expectation_prior = np.zeros((self._shape))
        self._expectation_uniform = np.zeros((self._shape))

        # set distribution parameters based on type of prior
        if self.simulator.dist = norm:
            self.par_1 = np.full(
                (len(self._outcomes), len(self._attributes)), .5)
            self.par_2 = np.ones((len(self._outcomes), len(self._attributes)))
        else self.simulator.dist = stu:
            # parameters to be determined

            # calculate a uniform prior for 1st MCMC step
            for x in np.nditer(_expectation_uniform, op_flags=['readwrite']):
                    x[...] = st.uniform.rvs()

        self.expectation_prior = self._expectation_uniform

    def update(self, utility_model=add, dist=norm, sampler=None):  # dist=self.simulator.dist
        """
            utility_model: The type of utility model to be used for updating (add or gai). [not sure this is needed]
            dist: The type of prior pdf that is assumed of each user.
            sampler: The type of sampler to be used for generating the posterior pdf.
        """
        self.response = self.simulator.elicitor.response

        if sampler is not None:
            self.sampler = sampler
        else:
            self.sampler = self.simulator.sampler.metropolis(self.data)

        # update of scaling vector - too tedious to elicit, a rank query of attributes is more practical

        # query outcomes and attributes in order to find the array position of the attribute/attribute value

        # update of distribution parameters
        for i in enumerate(self._attributes):
            par_1_post[i], par_2_post[i] = self.sampler

        # calculation of self.expectation_post
        self.expectation(dist, self.par_1,
                         self.par_2, self.expectation_post, scaling=None)

        # re-assignment of self.expectation_posterior as self.expectation_prior

        # calculation EVOI
        self.evoi(self.expectation_prior, self.expectation_post)

        # calculation of expected loss
        self.expected_loss()

    def expectation(self, dist=norm, par_1, par_2, output, scaling=None):
        """
        Args:
            dist: The type of distribution that expected values are being calculated for (norm or beta).
            par_1: The mean of the Gaussian distribution/The parameter a of the Beta distribution [array].
            par_2: The standard deviation of the Gaussian distribution/The parameter b of the Beta distribution [array].
            output: Name of returning array.
        """
        self._cum_expectation = np.zeros((len(self._outcomes, 1)))
        if scaling is not None:
            self.scaling = self.scaling_init
        else:
            self.scaling = scaling

        for i in enumerate(self._outcomes):
            for j in enumerate(self._attributes):
            if dist = norm:
                output[i, j] = st.norm(par_1[k], par_2[k]).expect()

        # apply the scaling variable to the expectation matrix

        # calculate total into a vector and then place it in the first column of output
        self._cum_expectation = np.cumsum(output, axis=1)
        output = np.vstack(self._cum_expectation, output[:1])

        return output

    def evoi():
