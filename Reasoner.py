# standard libraries
import math, operator
from functools import reduce

# third-party libraries
import numpy as np

class Reasoner:
	'''
	p : obj
	The problem instance.
	'''
	problem = None

	def __init__(self, p):
		self.problem = p
		self.max_eu = []
		self.prob =[]  # the cumulative probability of each response 
		# evaluated by computeResponses()

	# def generateUtilities(self, *level, p_type='param', sampler='metropolis'):
	# 	'''
	# 	This function approximates the utility value of each feature 
	# 	based on the response to the last query posed to the user and 
	# 	the set of samples being used to compute the approximation. 
	# 	Values for each possible response are stored in the list 
	# 	self.m_utilities for the metropolis sampler and self.i_utilities
	# 	 for the importance sampler.
	# 	Parameters
	# 	----------
	# 	*level : flt
	# 	The utility level being considered for a bound query. Possible
	# 	values range from .1 to .9.
	# 	p_type : str
	# 	The type of parameter that utilties are being computed for; 
	# 	either a feature or a utility level.
	# 	sampler : str
	# 	This is the set of samples being used in order to generate 
	# 	sample utilities of each possible response to a query 
	# 	(metropolis or importance). 
	# 	'''
	# 	q = len(self.problem.preferences)-1
	# 	r = self.problem.preferences[q]['response']
	# 	if sampler == 'metropolis':
	# 		utilities = self.m_utilities
	# 		samples = self.problem.metropolis
	# 	else:  # importance
	# 		utilities = self.i_utilities
	# 		if r == 1.:
	# 			samples = self.problem.importance[0]
	# 		elif r == 0.:
	# 			samples = self.problem.importance[1]
	# 	if p_type == 'param':
	# 		for i in range(self.problem.numParams()):
	# 			for j in range(len(samples)):
	# 				utilities[i].append(self.problem.evalUtility(
	# 					samples[j], i))
	# 	else:  # 'level'
	# 		for i in range(len(samples)):
	# 			utilities[len(utilities)-1].append(*level)

	def returnParams(self, query):
		'''
		This function takes a previous query and returns the features
		being considered.
		Parameters
		----------
		query : int
		This is the integer value of the previous query in the list
		self.preferences.
		Returns
		-------
		'''
		if type(self.problem.preferences[query]['query']).__name__ in \
                	('BoundQueryFlat', 'BoundQueryLinear'):
			params = [self.problem.preferences[query]['query'].x,
                	self.problem.preferences[query]['query'].l]
		else:  # 'ComparisonQueryFlat', 'ComparisonQueryLinear'
			params = [self.problem.preferences[query]['query'].x,
                    self.problem.preferences[query]['query'].y]
			if isinstance(params[0], tuple):  # local comparison query
				# logic not yet resolved
				pass
		return params

	def updateImportance(self, init, response):  
		# I think something is wrong with how the response probabilities
		# are applied to each sample
		# update: the probability of response to each query must be 
		# divided by the cumulative probability of response for each
		# possible response
		'''
		This function updates the walk self.importance based on all 
		queries posed to the user since the last metropolis sampling. 
		Parameters
		----------
		init : int
		The first query to be considered as far as user responses.
		'''
		prob = []
		samples = self.problem.metropolis.copy()
		for i in range(init-1, len(self.problem.preferences)):
			p = 0.0
			# print('the value of init is: ' + str(init))
			# print('the length of preferences is: ' + str(len(self.problem.preferences)))
			r = self.problem.preferences[i]['response']
			params = self.returnParams(i)
			if isinstance(params[1], float):  # global bound query
				if response == 'constant':
					for j in range(len(samples)):  
						if r == 1. and samples[j][params[0]] > [params[1]][0]:
							p += 1-self.noise
						else:
							p += self.noise
						if r == 0. and samples[j][params[0]] < [params[1]][0]:
							p += 1-self.noise
						else:
							p += self.noise
					# print('p is: ' + str(p))
					prob.append(p/len(samples))
				else:  # logistic
					for j in range(len(samples)):
						if r == 1.:
							p += math.exp(self.problem.gamma * samples[j][params[0]]) / \
								(math.exp(self.problem.gamma * samples[j][params[0]]) + \
								math.exp(self.problem.gamma * [params[1]][0]))
						if r == 0.:
							p += math.exp(self.problem.gamma * [params[1]][0]) / \
								(math.exp(self.problem.gamma * samples[j][params[0]]) + \
								math.exp(self.problem.gamma * [params[1]][0]))
					# print('p is: ' + str(p))
					prob.append(p/len(samples))
			elif isinstance(params[0], tuple) and isinstance(params[1], tuple):  # local comparison query
				# logic not yet resolved
				pass
			else:  # global comparison query
				if response == 'constant':
					for j in range(len(samples)):  
						if r == 1. and samples[j][params[0]] > samples[j][params[1]]:
							p += 1-self.noise
						else:
							p += self.noise
						if r == 0. and samples[j][params[0]] < samples[j][params[1]]:
							p += 1-self.noise
						else:
							p += self.noise
					# print('p is: ' + str(p))
					prob.append(p/len(samples))
				else:  # logistic
					for j in range(len(samples)):
						if r == 1.:
							p += math.exp(self.problem.gamma * samples[j][params[0]]) / \
							reduce(lambda x, y: x + y, [math.exp(self.problem.gamma *
        	                    samples[j][k]) for k in params])
						if r == 0.:
							p += math.exp(self.problem.gamma * samples[j][params[0]]) / \
							reduce(lambda x, y: x + y, [math.exp(self.problem.gamma *
        	                    samples[j][k]) for k in params])
					# print('p is: ' + str(p))
					prob.append(p/len(samples))
		# print('prob. list is: ' + str(prob))
		prob_c = np.prod(prob)  # cumulative probability
		# print('cum. prob. ' + str(prob_c))
		self.problem.importance.clear()
		for k in range(len(samples)):
			self.problem.importance.append(samples[k]*prob_c)

	def computeResponses(self, params, response, sampler):
		'''
		This function estimates the probabilities for each possible 
		query response ('yes' or 'no') based on a constant or
		softmax response model using a set of samples generated by the 
		chosen sampler.
		Parameters
		----------
		params : list
		The paramters being considered in a query.
		response : str
		The response model assumed.
		sampler : str
		The samples used to compute mean probabilities. 
		Returns
		-------
		v : int
		This is a list consisting of the two possible response estimates. 
		'''
		# this function should do the following:
		# 	-look at weights in self.importance/self.metropolis
		# 	-compute softmax probability of responses relevant to query
		# 	-average probabilities
		# 	-return probabilities for each possible response
		prob = []  # final response probabilities for each response
		self.prob = []  # the cumulative probability of each response 
		# evaluated by computeResponses()
		p = 0.0
		p_ = 0.0
		if sampler =='importance':
			samples = self.problem.importance
		else:  # metropolis
			samples = self.problem.metropolis
		if isinstance(params[1], float):  # global bound query
			if response == 'constant':
				for j in range(len(samples)):  
					if samples[j][params[0]] > [params[1]][0]:
						p += 1-self.noise
						p_ += self.noise
					else:
						p += self.noise
						p_ += 1-self.noise
				self.prob.append(p)
				self.prob.append(p_)
				# the cumulative response probabilities p and 1-p must
				# be accumulated and stored in self.p
				prob.append(p/len(samples))
				prob.append(1-(p/len(samples)))
			else:  # logistic	
				for j in range(len(samples)):
					p += math.exp(self.problem.gamma * samples[j][params[0]]) / \
						(math.exp(self.problem.gamma * samples[j][params[0]]) + \
						math.exp(self.problem.gamma * [params[1]][0]))
					p_ += math.exp(self.problem.gamma * [params[1]][0]) / \
                        (math.exp(self.problem.gamma * samples[j][params[0]]) +
                         math.exp(self.problem.gamma * [params[1]][0]))
				# print(str('the cumulative response probability for' + \
				# 	str(params[0]) + ' is: ' + str(p)))
				# print(str('the cumulative response probability for' +
                #     str(params[1]) + ' is: ' + str(p_)))  
				self.prob.append(p)
				self.prob.append(p_)
				prob.append(p/len(samples))
				prob.append(p_/len(samples))
				# prob.append(1-(p/len(samples)))
				# print('p + p_ sum to: ' + str(p + p_))
		elif isinstance(params[1], int):  # global comparison query
			if response == 'constant':
				for j in range(len(samples)):  
					if samples[j][params[0]] > samples[j][params[1]]:
						p += 1-self.noise
						p_ += self.noise
					else:
						p += self.noise
						p_ += 1-self.noise
				# the cumulative response probabilities p and 1-p must 
				# be accumulated and stored in self.p
				self.prob.append(p)
				self.prob.append(p_)
				prob.append(p/len(samples))
				prob.append(1-(p/len(samples)))
			else:  # logistic
				for j in range(len(samples)):
					p += math.exp(self.problem.gamma * samples[j][params[0]]) / \
						reduce(lambda x, y: x + y, [math.exp(self.problem.gamma * \
                        samples[j][k]) for k in params])
					p_ += math.exp(self.problem.gamma * samples[j][params[1]]) / \
                        reduce(lambda x, y: x + y, [math.exp(self.problem.gamma *
                        samples[j][k]) for k in params])
				# print(str('the cumulative response probability for ' +
                #     str(params[0]) + ' is: ' + str(p)))
				# print(str('the cumulative response probability for ' +
                #     str(params[1]) + ' is: ' + str(p_)))
				self.prob.append(p)
				self.prob.append(p_)
				prob.append(p/len(samples))
				prob.append(p_/len(samples))
				# prob.append(1-(p/len(samples)))
		# print('p + p_ sum to: ' + str(p + p_))
		elif isinstance(params[0], tuple) and isinstance(params[1], tuple):
			# local comparison query; logic not yet resolved
			pass
		return prob

	def computeWeights(self, params, response, sampler):
		'''
		This function estimates a vector of feature weights for each 
		possible response (yes/no) using the set of samples generated 
		by the metropolis or importance sampler.
		Parameters
		----------

		Returns
		-------
		w : list
		The first element is a set of feature weights corresponding to
		 the response 'yes'. The second element corresponds to the 
		 response 'no'.
		'''
		# this function should do the following:
		# 	-look at weights in self.importance/self.metropolis
		# 	-compute softmax probability of responses relevant to query
		#   -re-weight each sample for each  response
		# 	-average weights corresponding to each response
		# 	-return weights for each response
		w = []  # final mean weights for each response
		samples_ = [[], []]  # weights conditioned on each response
		if sampler == 'importance':
			samples = self.problem.importance
		else:  # metropolis
			samples = self.problem.metropolis
		if isinstance(params[1], float):  # global bound query
			if response == 'constant':
				for j in range(len(samples)):
					p = 0
					if samples[j][params[0]] > [params[1]][0]:
						p = 1-self.noise
						samples_[0].append(samples[j]*p)
						samples_[1].append(samples[j]*(1-p))
					else:
						p = 1-self.noise
						samples_[0].append(samples[j]*(1-p))
						samples_[1].append(samples[j]*p)
			else:  # logistic
				for j in range(len(samples)):
					p = 0
					p = math.exp(self.problem.gamma * samples[j][params[0]]) / \
                        (math.exp(self.problem.gamma * samples[j][params[0]]) +
                         math.exp(self.problem.gamma * [params[1]][0]))
					samples_[0].append(samples[j]*p)
					samples_[1].append(samples[j]*(1-p))
		elif isinstance(params[1], int):  # global comparison query
			if response == 'constant':
				for j in range(len(samples)):
					p = 0
					if samples[j][params[0]] > samples[j][params[1]]:
						p = 1-self.noise
						samples_[0].append(samples[j]*p)
						samples_[1].append(samples[j]*(1-p))
					else:
						p = 1-self.noise
						samples_[0].append(samples[j]*(1-p))
						samples_[1].append(samples[j]*p)
			else:  # logistic
				for j in range(len(samples)):
					p = 0
					p = math.exp(self.problem.gamma * samples[j][params[0]]) / \
                        reduce(lambda x, y: x + y, [math.exp(self.problem.gamma *
                        samples[j][k]) for k in params])
					samples_[0].append(samples[j]*p)
					samples_[1].append(samples[j]*(1-p))
		elif isinstance(params[0], tuple) and isinstance(params[1], tuple):
			# local comparison query; logic not yet resolved
			pass
		for i in [0, 1]:
			v = reduce(lambda x, y: x + y, [samples_[i][j] for j in range(len(samples_[i]))]) / \
				self.prob[i]
			# print('the estimated weight for ' + str(params[i]) + ' is: ' + str(v))
			# print('the cum. prob. of response (denomin.) is: ' + str(self.prob[i]))
			w.append(v)
		return w

	def maximizeEU(self, w, utility):
		'''
		This function computes the EU of the EU-maxmizing item.
		Parameters
		----------
		w : list
		This list contains the parameter weights for each 
		possible response (yes/no) using the set of samples generated 
		by the sampler.
		Returns
		-------
		eu : list
		The first element is the EU of the item corresponding to the 
		response 'yes'. The second element corresponds to the response 
		'no'.
		'''
		# change this logic to if utility; then for i in items; then 
		# return will work properly
		eu = []
		for i in range(self.problem.numItems()):
			v = 0
			if utility == 'linear':
				v = reduce(lambda x, y: x + y, [self.problem.evalUtility(
				w, (j, i)) for j in range(self.problem.numParams())])
				eu.append(v)
			else:  # 'flat'
			 	eu.append(self.problem.evalUtility(w, i))
		self.max_eu.append(max(eu))

	def computeEPU(self, params, response, sampler):
		'''
		This function computes the EUS of a query.
		Parameters
		----------
		params : list
		This list contains the features which are being considered in
		this query.
		sampler : str
		This is the set of samples being used in order to generate 
		sample utilities of each possible response to a query 
		(metropolis or importance).
		Returns
		-------
		v : flt
		The expected posterior utility of the query.
		'''
		r = self.computeResponses(params, response=response, \
			sampler=sampler)
		w = self.computeWeights(params, response=response, sampler=sampler)
		self.max_eu.clear()
		self.maximizeEU(w[0])
		self.maximizeEU(w[1]) 
		v = r[0] * self.max_eu[0] + r[1] * self.max_eu[1]
		# no idea why the equation below won't work with return statement
		# see comments in maximizeEU(); this way will work
		# v = r[0] * self.maximizeEU(w[0]) + r[1] * self.maximizeEU(w[1])
		# --------------------------------------------------------------
		# print('the probability of preferring param ' + str(params[0]) + \
		# 	' is: ' + str(r[0]))
		# print('the probability of preferring param ' + str(params[1]) +
        #     ' is: ' + str(r[1]))
		# print('the mean weight corresponding to a preference of param ' + 
		# 	str(params[0]) + ' is: ' + str(w[0]))
		# print('the mean weight corresponding to a preference of param ' +
        #     str(params[1]) + ' is: ' + str(w[1]))
		# print('the final EPU is: ' + str(v))
		return v

	# def computeEUS(self, params, response='logistic', sampler='importance'):
	# 	'''
	# 	This function computes the EUS of a query.
	# 	Parameters
	# 	----------
	# 	params : list
	# 	This list contains the features which are being considered in
	# 	this query.
	# 	sampler : str
	# 	This is the set of samples being used in order to generate 
	# 	sample utilities of each possible response to a query 
	# 	(metropolis or importance).
	# 	Returns
	# 	-------
	# 	'''
	# 	r = self.computeResponses(params, response=response, \
	# 		sampler=sampler)
	# 	w = self.computeWeights()
	# 	p = []
	# 	p.append(params[0])
	# 	if isinstance(params[1], float):
	# 		p.append(params[0])
	# 	else:  # int, tuple
	# 		p.append(params[1])
	#	not even the right calculation
	#	v = r[0] * self.problem.evalUtility(w[0], p[0]) + \
	#		r[1] * self.problem.evalUtility(w[1], p[1])
	#	return v


	def computeQuestion(self, response='logistic', sampler='metropolis'):
		'''
		This function computes the next question to be posed to the user.
		Parameters
		----------
		sampler : str
		This is the set of samples being used in order to generate
		sample utilities of each possible response to a query
		(metropolis or importance).
		'''
		# next functionality: account for local queries
		# can returnParams() be used here? no.
		for key in self.problem.queries:
			params = []
			x = key.x
			if isinstance(x, tuple):
				params.append(x[0])
			else:  # int
				params.append(x)
			if type(key).__name__ in ('BoundQueryLinear', 'BoundQueryFlat'):
				params.append(key.l)
				self.problem.queries[key] = self.computeEPU(
					params, response=response, sampler=sampler)
			elif type(key).__name__ in ('ComparisonQueryLinear',
										'ComparisonQueryFlat'):
				y = key.y
				if isinstance(x, tuple):
					params.append(y[0])
				else:  # int
					params.append(y)
				self.problem.queries[key] = self.computeEPU(
					params, response=response, sampler=sampler)
		q = max(self.problem.queries.items(), key=operator.itemgetter(1))[0]
		self.problem.question = q
		print('The most informative next question is: ' + str(self.problem.question))


class ReasonerFlat(Reasoner):
	def __init__(self,p):
		super().__init__(p)

	def maximizeEU(self, w, utility='flat'):
		super().maximizeEU(w, utility)


class ReasonerLinear(Reasoner):
	def __init__(self, p):
		super().__init__(p)

	def maximizeEU(self, w, utility='linear'):
		super().maximizeEU(w, utility)
